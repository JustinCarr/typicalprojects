// Dank ass JS

(function($) {
    "use strict";

    $("a:not(.ignoreBlur), button, .btn-social, .btn, .navbar-toggle").click(function(){
        $(this).blur();
    });

    $("a:not(.ignoreBlur), button, .btn-social, .btn, .navbar-toggle").click(function(){
        $(this).removeClass("active");
    });

    $('#profileModal').on('show.bs.modal', function(e){
        $('.myworks-link').one('focus', function(e){$(this).blur();});
    });

    $(".navbar-toggle").bind( "touchend", function(e){
        $(this).blur();
    });

    $('body').click(function(evt){
        if ($(evt.target).hasClass("navbar-toggle")) {
            return;
        }
        if ($('#bs-example-navbar-collapse-1').attr('aria-expanded') === "true") {
            if(evt.target.id == "bs-example-navbar-collapse-1")
                return;
            //For descendants of menu_content being clicked, remove this check if you do not want to put constraint on descendants.
            if($(evt.target).closest('#bs-example-navbar-collapse-1').length)
                return;
            $('.navbar-toggle:visible').click();
        }

    });

    var scrollHandling = {
        allow: true,
        reallow: function() {
            scrollHandling.allow = true;
        },
        delay: 1000 //(milliseconds) adjust to the highest acceptable value
    };

    $(window).scroll(function() {
        if(scrollHandling.allow) {
            if ($('#bs-example-navbar-collapse-1').attr('aria-expanded') === "true") {
                $('.navbar-toggle:visible').click();
            }
            scrollHandling.allow = false;
            setTimeout(scrollHandling.reallow, scrollHandling.delay);
        }
    });

    // jQuery for page scrolling feature - jQuery Easing plugin
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - $(".navbar-header").outerHeight())
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    });

    // Close menu on click
    $('.navbar-collapse ul li a:not(.dropdown-toggle)').click(function() {
        $('.navbar-toggle:visible').click();
    });

    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

    // Floating label headings for the contact form
    $(function() {
        $("body").on("input propertychange", ".floating-label-form-group", function(e) {
            $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
        }).on("focus", ".floating-label-form-group", function() {
            $(this).addClass("floating-label-form-group-with-focus");
        }).on("blur", ".floating-label-form-group", function() {
            $(this).removeClass("floating-label-form-group-with-focus");
        });
    });

})(jQuery); // End of use strict
