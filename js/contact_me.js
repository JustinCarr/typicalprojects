$(function() {

    var body = document.body,
        html = document.documentElement;

    var height = Math.max( body.scrollHeight, body.offsetHeight,
                           html.clientHeight, html.scrollHeight, html.offsetHeight );
    var tempScrollBottom = height - $(window).scrollTop();

    $("#contactForm input,#contactForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            // Prevent spam click and default submit behaviour
            $("#btnSubmit").attr("disabled", true);
            event.preventDefault();

            // get values from FORM
            var name = $("input#name").val();
            var email = $("input#email").val();
            var subject = $("input#subject").val();
            var message = $("textarea#message").val();
            var captchaResponse = grecaptcha.getResponse();

            var firstName = name; // For Success/Failure Message
            // Check for white space in name for Success/Fail message
            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
            $.ajax({
                url: "././mail/contact_me.php",
                type: "POST",
                data: {
                    name: name,
                    subject: subject,
                    email: email,
                    message: message,
                    gRecaptchaResponse: captchaResponse
                },
                cache: false,
                success: function(data) {
                    // Enable button & show success message

                    if (data == "Code 1") {

                        $('#submitButtonID').delay(2000).hide();
                        $('#captchaID').delay(2000).hide();

                        $('#success').hide();
                        $('#success').html("<div class='alert alert-success'>");
                        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                        $('#success > .alert-success')
                            .append("<strong>Your message has been sent. </strong>");
                        $('#success > .alert-success')
                            .append('</div>');
                        $('#success').show(2000);
                        $('#contactForm').delay(2000).trigger("reset");

                    } else {

                        $("#btnSubmit").attr("disabled", false);

                        $('#success').html("<div class='alert alert-danger'>");
                        $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                        $('#success > .alert-danger').append("<strong>Sorry " + firstName + ", ");

                        if (data == "Code 2") {

                            // Didn't enter captcha
                            $('#success > .alert-danger').append("you need to complete the captcha!");

                        } else if (data == "Code 3") {

                            // Entered captcha, but failed
                            $('#success > .alert-danger').append("you failed the captcha! Try again.");

                        } else if (data == "Code 4") {

                            // POST data doesn't validate, because you tampered with the js....
                            $('#success > .alert-danger').append("we have out-smarted you!");

                        } else {

                            // Something went wrong with the mail server.
                            $('#success > .alert-danger').append("it seems that my mail server is not responding. Please try again later!");
                            $('#contactForm').trigger("reset");
                        }

                        $('#success > .alert-danger').append('</div>');

                    }
                    grecaptcha.reset();

                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + ", it seems that my mail server is not responding. Please try again later!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                    grecaptcha.reset();

                },
            });
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    height = Math.max( body.scrollHeight, body.offsetHeight,
                           html.clientHeight, html.scrollHeight, html.offsetHeight );

    $(window).scrollTop(height - tempScrollBottom);

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});

// When clicking on Full hide fail/success boxes
$('#name').focus(function() {
    $('#success').html('');
});
