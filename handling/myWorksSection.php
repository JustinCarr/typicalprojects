<!-- My Works Grid Section -->
<!--
EXTRA HTML for end of file
<ul class="list-inline item-details">
    <li>Some seciton:
        <strong><a href="link">content</a>
        </strong>
    </li>
    <li>Some Other Section:
        <strong><a href="link">content</a>
        </strong>
    </li>
    <li>Some OTHER Section:
        <strong><a href="link">content</a>
        </strong>
    </li>
</ul>
 -->
<?php
    require_once 'constants.php';
    require (C("fileWorkBuilder"));
    $myWorkDivTemplate = '
        <div class="col-sm-4 myworks-item">
            <a href="#myworksModal%u" class="myworks-link" data-toggle="modal">
                <div class="caption">
                    <div class="caption-content">
                        <i class="fa fa-search-plus fa-3x"></i>
                        </div>
                    </div>
                <img src="img/myworks/%s" class="img-responsive" alt="">
            </a>
        </div>';
    $myWorkContentTemplate = '
        <div class="myworks-modal modal fade" id="myworksModal%u" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <h2>%s</h2>
                                <hr class="star-primary">
                                <img src="img/myworks/%s" class="img-responsive img-centered" alt="">
                                %s
                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
?>

<section id="myworks">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2><?php echo C("myProjectsSec");?></h2>
                <hr class="star-primary">
            </div>
        </div>

        <div class="row">
            <?php

                buildWorks();
                $counter = 1;
                foreach($works as $work) {
                    printf($myWorkDivTemplate, $counter, $work->getIconName());
                    $counter++;
                }
            ?>
        </div>
    </div>
</section>

<?php
    $counter = 1;

    foreach($works as $work) {
        printf($myWorkContentTemplate, $counter, $work->getThisWorksName(), $work->getIconName(), file_get_contents($work->getContentFile()));
        $counter++;
    }
?>
