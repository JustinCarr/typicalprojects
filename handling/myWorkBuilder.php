<?php

$works = array();


class Work {

    private $workName = "";
    private $imgName = "";
    private $imgPath = "";
    private $content = "";

    public function __construct($workName, $imgName, $imgPath, $content) {
        $this->workName = $workName;
        $this->imgName = $imgName;
        $this->imgPath = $imgPath;
        $this->content = $content;
    }

    public function getThisWorksName() {
        return $this->workName;
    }

    public function getIconName() {
        return $this->imgName;
    }

    public function getIconPath() {
        return $this->imgPath;
    }

    public function getContentFile() {
        return $this->content;
    }

}

function buildWorks() {
    global $works;

    $infoFile = fopen(C("fileWorksSummary"), "r");

    $name = null;
    $img = null;
    $content = null;
    require_once 'constants.php';

    while (($line = fgets($infoFile)) !== false) {


        if (strpos($line, "=====") !== false) {
            if ($img !== null && $content !== null && $name !== null) {
                array_push($works, new Work($name, $img, C("dirImgMyWorks") . $img, C("dirMyWorks") . $content));
            }
            $name = null;
            $img = null;
            $content = null;
            continue;
        } else if (substr($line, 0, 1) === "#") {
            continue;
        }

        $specifier = explode(": ", $line, 2);
        throwExIfNotRightLength($specifier, 2);
        $specifier[1] = trim($specifier[1]);

        switch ($specifier[0]) {
            case "Name":
                $name = $specifier[1];
                break;
            case "Icon_File_Name":
                $img = $specifier[1];
                break;
            case "IncludeFile":
                $content = $specifier[1];
                break;
            default:
                break;
        }

    }

    fclose($infoFile);

}

function throwExIfNotRightLength($array, $length) {
    if (count($array) !== $length) {
        throw new Exception("Array index out of bounds, TELL JUSTIN!");
    }
}
?>
