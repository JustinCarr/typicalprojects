<!-- About Section -->
<section class="success" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>About</h2>
                <hr class="star-light">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <p>I, Justin Carrington, made this site to host some school and extra-curricular
                    things, like my tutoring websites, transcripts, and running plans.</p>
            </div>
            <div class="col-lg-8 col-lg-offset-2">
                <p>If you'd like to get in contact, for whatever reason, you can use the contact form below!</p>
            </div>
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <a href="#profileModal" class="btn btn-lg btn-outline myworks-link" data-toggle="modal">
                    <i class="fa fa-map-o"></i>    More About Me
                </a>
            </div>
        </div>
    </div>
</section>
