<html>
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#page-top">Typical Projects</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <?php
                        // ($_SERVER["PHP_SELF"] === "index.php" ? '' : '/')
                        $redirect_char = ($_SERVER["PHP_SELF"] == "/index.php") ? '' : '/';
                    ?>
                    <li class="hidden">
                        <a class="ignoreBlur" href="<?php echo $redirect_char;?>#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a class="ignoreBlur" href="<?php echo $redirect_char;?>#myworks">My Works</a>
                    </li>
                    <li class="page-scroll">
                        <a class="ignoreBlur" href="<?php echo $redirect_char;?>#about">About</a>
                    </li>
                    <li class="page-scroll">
                        <a class="ignoreBlur" href="<?php echo $redirect_char;?>#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

</html>
