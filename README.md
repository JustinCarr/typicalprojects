# ** THIS IS THE REPO FOR THE SITE HOSTED AT TYPICALPROJECTS.COM ** #

# Workflow #

* Changes are made on Laravel Homestead instance running on Vagrant VM.
* These changes are committed and pushed to this repo
* Repo is pulled down on the production server

# Technical description #

* Run startup script for Laravel Homestead
* In the laravel project in that vagrant vm, put website files in public. This is where this repo is cloned.
* Push changes to this repo when done editing, either via Terminal or Atom
* Use SSH script to get onto production server
* execute the sync script (./sync), password is for git user
* exit, process complete