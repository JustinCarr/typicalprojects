<!DOCTYPE html>
<html lang="en">

<?php require_once 'constants.php'; ?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Hub page for typical projects">
    <meta name="author" content="Justin Carrington">

    <title><?php echo $cs['siteTitle']; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- CSS -->
    <link href="css/typical.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>

<body id="page-top" class="index">

    <?php
        include 'includes/navGlobal.php';
    ?>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" src="img/profile.png" alt="">
                    <div class="intro-text">
                        <span class="name"><?php echo $cs['siteTitle']; ?></span>
                        <hr class="star-light">
                        <span class="skills"><?php echo $cs['mainSlogan']; ?></span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- My Works Section -->
    <?php include("handling/myWorksSection.php");?>

    <!-- About Section -->
    <?php include($cs['fileMeSummary']); ?>

    <!-- Profile Modal -->
    <div class="myworks-modal modal fade" id="profileModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>About Me</h2>
                            <hr class="star-primary">
                            <img src="img/myworks/cabin.png" class="img-responsive img-centered" alt="">
                            Some future description.
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Contact Me</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">

                    <form name="sentMessage" id="contactForm" method="post" novalidate>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Name" id="name" required data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Email Address</label>
                                <input type="email" class="form-control" placeholder="Email Address" id="email" required data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Subject</label>
                                <input type="text" class="form-control" placeholder="Subject" id="subject" required data-validation-required-message="Please enter a message subject.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Message</label>
                                <textarea type= "message" rows="5" class="form-control" placeholder="Message" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div id="captchaID" class="row control-group">
                            <br>
                            <div class="g-recaptcha" data-sitekey="6LexmycTAAAAADoZCuLVWQ8l3j9EGICq94ALGuK4"></div>

                        </div>
                        <br>
                        <div id="success"></div>
                        <div id="submitButtonID" class="row">
                            <div class="form-group col-xs-12">
                                <button type="submit" class="btn btn-success btn-lg">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <?php include("includes/footer.html");?>

    <!-- Import everything we need for javascript -->
    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Ajax -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- jQuery Boostrap Validation Plugin -->
    <script src="js/jqBootstrapValidation.js"></script>

    <!-- Main JavaScript -->
    <script src="js/contact_me.js"></script>
    <script src="js/typical.min.js"></script>

</body>

</html>
