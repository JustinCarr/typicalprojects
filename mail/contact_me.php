<?php
// Check for invalid fields
// check reCAPTCHA information also
// NOTE:
// Code 1 = success
// Code 2 = recaptcha wasn't submitted
// Code 3 = recaptcha was submitted and was invalid
// Code 4 = code was tampered with in browser. Deny.

if ($_POST['gRecaptchaResponse'] == null) {
   echo "Code 2";
   return false;
}
$resp = isValid($_POST['gRecaptchaResponse']);

// if CAPTCHA is correctly entered!                        
if ($resp != null && $resp == true) {                        
   if(empty($_POST['name'])      ||
      strlen($_POST['name']) === 50 ||
      empty($_POST['email'])     ||
      strlen($_POST['email']) === 50 ||
      empty($_POST['subject'])     ||
      empty($_POST['message'])   ||
      strlen($_POST['message']) === 500 ||
      !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
      echo "Code 4";
      return false;
   }
   
   $name = strip_tags(htmlspecialchars($_POST['name']));
   $email_address = strip_tags(htmlspecialchars($_POST['email']));
   $subject = strip_tags(htmlspecialchars($_POST['subject']));
   $message = strip_tags(htmlspecialchars($_POST['message']));
      
   // Create the email and send the message
   $to = 'justin@typicalprojects.com';
   $email_subject = "TP Home Contact: $name";
   $email_body = "<html><body><h2>Contact Request - Typical Projects Home </h1>\r\n \r\n" .
          "<strong>===========================================</strong>\r\n" .
          "<h3><strong>Details</strong></h3>\r\n" .
          "<ul><li><strong>Name</strong>: $name</li>\r\n" . 
          "<li><strong>Email address</strong>: $email_address</li>\r\n" . 
          "<li><strong>Subject</strong>: $subject</li></ul>\r\n " . 
          "<strong>------------------------------------------</strong>\r\n" . 
          "<h3><strong>Message</strong></h3>\r\n" . 
          "<ul><li>$message</li></ul>\r\n" . 
          "<strong>==========================================</strong></body></html>";

   $headers = "From: justin@typicalprojects.com\r\n";
   $headers .= "Reply-To: $email_address\r\n";
   $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
   mail($to,$email_subject,$email_body,$headers);
   echo "Code 1";
   return true;   
} else {
   
   echo "Code 3";
   return false;
}

function isValid($captchaResp) 
{
    try {

        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = ['secret'   => '6LexmycTAAAAACIET9xPrkQfGECjEXuAetvjqGZ2',
                 'response' => $captchaResp,
                 'remoteip' => $_SERVER['REMOTE_ADDR']];

        $options = [
            'http' => [
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data) 
            ]
        ];

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return json_decode($result)->success;
    }
    catch (Exception $e) {
        return null;
    }
}

?>
