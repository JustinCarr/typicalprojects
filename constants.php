<?php

/*
Holds the constants used throughout the website
*/
$r = $_SERVER['DOCUMENT_ROOT'];

if ($r[strlen($r) - 1] !== "/") {
    $r = $r . "/";
}

$cs = array(
    // Main site
    "siteTitle"         => "Typical Projects",
    "mainSlogan"        => "Justin's Life (Or Not-So-Typical) Toolbox",
    "myProjectsSec"     => "My Works",
    // Error pages
    "404Title"          => "Whoopsie... 404",
    "404Slogan"         => "Such Confuse - Find Not - Wow",
    "403Title"          => "Whoopsie... 403",
    "403Slogan"         => "Many Security - See Not - Wow",
    // Paths
    "fileWorksSummary"  => $r . "works/works_summary.txt",
    "fileWorkBuilder"   => $r . "handling/myWorkBuilder.php",
    "dirImgMyWorks"     => $r . "img/myworks/",
    "dirMyWorks"        => $r . "works/",
    "fileMeSummary"     => $r . "about.php"
);

function C($node) {
    return $GLOBALS['cs'][$node];
}

?>
