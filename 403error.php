<!DOCTYPE html>
<html lang="en">

<?php require_once 'constants.php'; ?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="403 Error - Access Forbidden">
    <meta name="author" content="Justin Carrington">

    <title><?php echo $cs["siteTitle"] ?> - 403</title>

    <!-- Bootstrap Core CSS -->
    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- CSS -->
    <link href="/css/typical.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>

<body id="page-top" class="index">

    <!-- Nav -->
    <?php

        $path = $_SERVER['DOCUMENT_ROOT'];
        $path .= "/includes/navGlobal.php";
        include($path);
    ?>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" src="/img/profile.png" alt="">
                    <div class="intro-text">
                        <span class="name"><?php echo $cs["403Title"] ?></span>
                        <hr class="star-light">
                        <span class="skills"><?php echo $cs["403Slogan"] ?></span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Footer -->
    <?php

        $path = $_SERVER['DOCUMENT_ROOT'];
        $path .= "/includes/footer.html";
        include($path);
    ?>

    <!-- Import everything we need for javascript -->
    <!-- jQuery -->
    <script src="/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Ajax -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- jQuery Boostrap Validation Plugin -->
    <script src="/js/jqBootstrapValidation.js"></script>

    <!-- Main JavaScript -->
    <script src="/js/contact_me.js"></script>
    <script src="/js/typical.min.js"></script>


</body>

</html>
